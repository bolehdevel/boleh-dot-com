import React from 'react';
import {View, FlatList, Image, TouchableOpacity} from 'react-native';
import {Card, Title, Paragraph} from 'react-native-paper';
import moment from 'moment';
import HTML from 'react-native-render-html';
export default class CategorieList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      links:null,
      image: null,
    };
  }
  componentDidMount() {
   this.fetchPost();
   //this.renderRow();
   //this.getImage();
 }
 getImage = () => {
   const posts = this.state.posts;
   const links = posts._links['wp:featuredmedia'];
   console.log("navigasi:",links);
   if (links) {
     fetch(links[0].href)
       .then(response => response.json())
       .then(responseData => {
         // this.setState() will cause the new data to be applied to the UI that is created by the `render` function below.
         this.setState({ image: responseData.guid.rendered });
       })
       .done();
   }
 };
 async fetchPost() {
   let categorie_id = this.props.navigation.getParam('categorie_id');
   const response = await fetch(
     `http://boleh.com/wp-json/wp/v2/posts?categories=${categorie_id}`,
   );
   const post = await response.json();
   this.setState({posts: post});
   //this.getImage();
   //console.log("post catagorie:", this.state.posts._links['wp:featuredmedia']);
 }

 renderRow = ({item}) =>{
   var idLocale = require('moment/locale/id');
  moment.updateLocale('id', null);
   const links = item._links['wp:featuredmedia'];
   //console.log("navigasi:",links);
   if (links) {
     fetch(links[0].href)
       .then(response => response.json())
       .then(responseData => {
         // this.setState() will cause the new data to be applied to the UI that is created by the `render` function below.
         this.setState({ image: responseData.guid.rendered });
       })
       .done();

   }
   return(
     <TouchableOpacity
       onPress={() =>
         this.props.navigation.navigate('SinglePost', {
           post_id: item.id,
         })
       }>
       <Card>
         <Card.Content>
           <Title>{item.title.rendered}</Title>
           <Paragraph>
             Published on {moment(item.date, 'YYYYMMDD').fromNow()}
           </Paragraph>
         </Card.Content>
         <Image
         style={{
           width: '100%',
           // height: 100,
           aspectRatio: 1,
           borderTopLeftRadius: 3,
           borderTopRightRadius: 3,
           overflow: 'hidden',
           // position: 'absolute',
           // top: 0,
           // left: 0,
         }}
          source={{
           uri: item._links['wp:featuredmedia'][0].href}} />
         <Card.Content>
           <HTML html={item.excerpt.rendered} />
         </Card.Content>
       </Card>
     </TouchableOpacity>
   );
 }
 render() {
  const categorie_title = this.props.navigation.getParam('categorie_name');
    return (
      <View>
        <Title style={{marginLeft: 30}} >{categorie_title}</Title>
        <FlatList
          data={this.state.posts}
          renderItem={this.renderRow
            //({item}) => (

            //<TouchableOpacity
              //onPress={() =>
                //this.props.navigation.navigate('SinglePost', {
                  //post_id: item.id,
                //})
              //}>
              //<Card>
                //<Card.Content>
                  //<Title>{item.title.rendered}</Title>
                  //<Paragraph>
                    //Published on {moment(item.date, 'YYYYMMDD').fromNow()}
                  //</Paragraph>
                //</Card.Content>
                //<Card.Cover source={{
                  //uri: item._links['wp:featuredmedia'][0].href}} />
                //<Card.Content>
                  //<HTML html={item.excerpt.rendered} />
                //</Card.Content>
              //</Card>
            //</TouchableOpacity>
          //)
        }
          keyExtractor={item => item.id.toString()}
        />
      </View>
    );
  }
}
