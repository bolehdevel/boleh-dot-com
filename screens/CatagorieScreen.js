import React from 'react';
import {
  FlatList,
  ScrollView,
  View,
  Image,
  TouchableOpacity, StatusBar, ActivityIndicator
} from 'react-native';
import * as Font from 'expo-font';
import {
  Avatar,
  Button,
  Card,
  Title,
  Paragraph,
  List,
} from 'react-native-paper';
import {
  createStackNavigator,
  createAppContainer
} from 'react-navigation';
import CatagoriePriview from './Catagorie';
// WP REST API
const REQUEST_URL_CAT = 'http://boleh.com/wp-json/wp/v2/categories';
export class CatagorieScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      categories: null,
      assetsLoaded: false,
    };
  }
  componentDidMount() {
    this.fetchCategorie();
    Font.loadAsync({
            'bebas': require('../assets/fonts/SpaceMono-Regular.ttf')
        });
        this.setState({ assetsLoaded: true });
  }

  async fetchCategorie() {
    this.setState({ loading: true });
    //const response = await fetch(`https://kriss.io/wp-json/wp/v2/categories`);
    //const categories = await response.json();
    await fetch(REQUEST_URL_CAT)
      .then(response => response.json())
      .then(responseData => {
        // this.setState() will cause the new data to be applied to the UI that is created by the `render` function below.
        this.setState({
          categories: responseData
        });

      })
      .done();

  }
  render() {
    //console.log("links", this.state.categories);
    //const links = this.state.catagories._links['wp:featuredmedia'];
    return (

      <ScrollView>
        <FlatList
          data={this.state.categories}
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('CatagoriePriview', {
                  categorie_id: item.id,
                  categorie_name: item.name,
                  //catagorie_links:item._links['wp:featuredmedia']
                })
              }>
              <View
                style={{
                  borderColor: '#aaaaaa',
                  borderWidth: 0.25,
                  backgroundColor: '#ffffff',
                  marginHorizontal: 20,
                  // marginLeft: 20,
                  marginTop: 20,
                  marginBottom: 0,
                  borderRadius: 3,
                  elevation: 3,
                  // minWidth: itemWidth,
                  // maxWidth: itemWidth,
                  // height: itemHeight,
                }}>
              <Card>
                <Card.Content>
                  <Title style={{
                    fontFamily: 'bebas',
                    fontSize: 30,
                    //fontWeight: 'bold',
                    // color: 'white',
                    padding: 10,
                    // backgroundColor: 'rgba(0, 0, 0, 0.6)',
                    // borderRadius: 5,
                    textAlign: 'center',
                  }}>{item.name}</Title>
                </Card.Content>
              </Card>
              </View>
            </TouchableOpacity>
          )}
          keyExtractor = { (item, index) => index.toString() }
        />
      </ScrollView>
    );
  }

}
const MainNav = createStackNavigator(

  {

    CatagorieScreen: {
      screen:CatagorieScreen,
    },
    CatagoriePriview: {
      screen: CatagoriePriview,
      //navigationOptions: {
        //title: this.state.categories.name,
      //},
    },
  },
  {
    initialRouteName: 'CatagorieScreen',
    mode: 'modal',
    navigationOptions: {
      title: 'Catagories',
    },
  }
);

const PostsNav = createAppContainer(MainNav);

export default PostsNav;

CatagorieScreen.navigationOptions = {
  headerTitle: (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
  <Image
  resizeMode ="cover"
  style={
    {
      width: 180,
      height: 90,
      resizeMode:'contain',
      alignItems: 'center'
    }
  }
  source={require('../assets/images/logo.png')}/>
  </View>
)
};
CatagoriePriview.navigationOptions = {
  headerTitle: (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
  <Image
  resizeMode ="cover"
  style={
    {
      width: 180,
      height: 90,
      resizeMode:'contain',
      alignItems: 'center'
    }
  }
  source={require('../assets/images/logo.png')}/>
  </View>
)
};
