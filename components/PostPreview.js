import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Image, Dimensions, StatusBar, ActivityIndicator } from 'react-native';
import { AppLoading } from "expo";
import * as Font from 'expo-font'
const {height, width} = Dimensions.get('window');
const itemWidth = (width - 60) / 2;
const itemHeight = (height - 50) / 2;

export default class PostPreview extends Component {
  state = {
    image: null,
    assetsLoaded: false,
  };
  componentWillMount() {
         this._loadAssetsAsync();
       }
       _loadAssetsAsync = async () => {
       await Font.loadAsync({
         bebas: require('../assets/fonts/Bebas-Regular.ttf')
       });
       this.setState({ assetsLoaded: true });
     };

  componentDidMount = () => {


    const links = this.props.data._links['wp:featuredmedia'];
    //console.log("links:",links);
    if (links) {
      fetch(links[0].href)
        .then(response => response.json())
        .then(responseData => {
          // this.setState() will cause the new data to be applied to the UI that is created by the `render` function below.
          this.setState({ image: responseData.guid.rendered });
        })
        .done();

    }
  };
  //
  render() {
    const {assetsLoaded} = this.state;
    //console.log("font :", this._loadAssetsAsync());
    if (!assetsLoaded) {
        return <AppLoading />;
      }
      //console.log("images satu:", this.state.image);

        return (


            <View
              style={{
                borderColor: '#aaaaaa',
                borderWidth: 0.25,
                backgroundColor: '#ffffff',
                marginHorizontal: 20,
                // marginLeft: 20,
                marginTop: 20,
                marginBottom: 0,
                borderRadius: 3,
                elevation: 3,
                // minWidth: itemWidth,
                // maxWidth: itemWidth,
                // height: itemHeight,
              }}>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate('Post', {
                    data: this.props.data,
                    copertina: this.state.image
                      ? this.state.image
                      : 'https://s.w.org/images/home/swag_col-1.jpg',
                  })
                }>
              <View
                style={{
                  // aspectRatio: 1,
                }}>
                <Image
                  style={{
                    width: '100%',
                    // height: 100,
                    aspectRatio: 1,
                    borderTopLeftRadius: 3,
                    borderTopRightRadius: 3,
                    overflow: 'hidden',
                    // position: 'absolute',
                    // top: 0,
                    // left: 0,
                  }}
                  source={{ uri: this.state.image }}
                />
                <Text
                  style={{
                    fontFamily: 'bebas',
                    fontSize: 20,
                    // color: 'white',
                    padding: 10,
                    // backgroundColor: 'rgba(0, 0, 0, 0.6)',
                    // borderRadius: 5,
                    textAlign: 'center',
                  }}>
                  {this.props.data.title.rendered}
                </Text>
              </View>
              </TouchableOpacity>
            </View>

        );
       }



}
